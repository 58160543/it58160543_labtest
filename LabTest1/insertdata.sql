INSERT INTO Department VALUES ('D001','Accounting');
INSERT INTO Department VALUES ('D002','Information Technology');
INSERT INTO Department VALUES ('D003','Maketing');

INSERT INTO Manager VALUES ('T001','Aunchasa','Rukdee','D001');
INSERT INTO Manager VALUES ('T002','Sarayut','Pattanun','D002');
INSERT INTO Manager VALUES ('T003','Panuwat','Sakdaporn','D003');

INSERT INTO Employee VALUES ('58161111','Petchara','Bunlerd','D001','T001');
INSERT INTO Employee VALUES ('58162222','Jerapat','Pansrida','D002','T002');
INSERT INTO Employee VALUES ('58163333','Suchada','Buasri','D003','T003');

